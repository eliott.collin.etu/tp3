export default class Router {
	static titleElement;
	static contentElement;
	/**
	 * Tableau des routes/pages de l'application.
	 * @example `Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }]`
	 */
	static routes = [];

	static #menuElement; // propriété statique privée
	/**
	 * Indique au Router la balise HTML contenant le menu de navigation
	 * Écoute le clic sur chaque lien et déclenche la méthode navigate
	 * @param element Élément HTML qui contient le menu principal
	 */
	static set menuElement(element) { // setter
		this.#menuElement = element;

		this.#menuElement.querySelectorAll("a").forEach(e => {
			const path = e.getAttribute('href')
			e.addEventListener('click', event => {
				event.preventDefault();

				this.#menuElement.querySelector(`a[href='${document.location.pathname}']`).classList.remove('active');
				Router.navigate(path, true);
				event.currentTarget.classList.add('active');
			});
		});
	}


	/**
	 * Affiche la page correspondant à `path` dans le tableau `routes`
	 * @param {String} path URL de la page à afficher
	 */
	static navigate(path, pushState) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			if(pushState)
				window.history.pushState(null, null, path);
			// affichage du titre de la page
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			// affichage de la page elle même
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
		}
	}
}
