import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from "./components/Component";
import PizzaForm from "./pages/PizzaForm";

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
    aboutPage = new Component('section', null, 'Ce site est génial'),
    pizzaForm = new PizzaForm(undefined, undefined);

Router.routes = [
    { path: '/', page: pizzaList, title: 'La carte' },
    { path: '/a-propos', page: aboutPage, title: 'À propos' },
    { path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.menuElement = document.querySelector('.mainMenu');
Router.navigate(document.location.pathname, true); // affiche la liste des pizzas

document.querySelector('.logo').innerHTML += "<small>les pizzas c'est la vie</small>";

document.querySelector(`a[href='${document.location.pathname}']`).classList.add('active');

const newContainer = document.querySelector('.newsContainer');

//newContainer.setAttribute('style', "display:''");
//newContainer.style = '';
newContainer.removeAttribute('style');

newContainer.querySelector('.closeButton').addEventListener('click', () => {
    newContainer.setAttribute('style', 'display:none');
});


window.onpopstate = () => {
    console.log(document.location.pathname);
    Router.navigate(document.location.pathname, false);
};
