import Page from './Page.js';

export default class PizzaForm extends Page {
    render() {
        return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
    }

    mount(element) {
        super.mount(element);
        this.element.addEventListener('submit', event => {
            event.preventDefault();
            const input = this.element.querySelector('input[name=name]');

            if (input.value === "") alert("Error field name is empty")
            else alert(`${input.value} pizza was added`);
            input.value = "";
        });
    }

    submit(event) {}
}